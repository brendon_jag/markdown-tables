# Table Test

Attempting to use the syntax indicated in the [Bitbucket documentation](https://confluence.atlassian.com/display/BITBUCKET/Displaying+README+Text+on+the+Overview), but it fails.

## Tables with no dashed lines
| HEADING | HEADING |
| Row text | Row text |


## Tables with dashed lines
-------------------------
| HEADING  | HEADING    |
-------------------------
| Row text | Row text   |
-------------------------
